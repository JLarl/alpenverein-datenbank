import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class DBManager {
	private Connection c;
	Scanner sc = new Scanner(System.in);
	
	public DBManager()
	{
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:\\sqlite\\sqlite-tools-win32-x86-3150100\\zug.db");
		}
		catch(SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void createTWaggon()
	{
		PreparedStatement stmt;
		String sql ="CREATE TABLE IF NOT EXISTS Waggon("+
					"waggonID INTEGER PRIMARY KEY AUTOINCREMENT,"+
					"klasse INT NOT NULL);";
		try {
			stmt=c.prepareStatement(sql);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void createTSitzplatz()
	{
		PreparedStatement stmt;
		String sql ="CREATE TABLE IF NOT EXISTS Sitzplatz("+
					"sitzplatz INTEGER PRIMARY KEY AUTOINCREMENT,"+
					"FS_waggonID INT NOT NULL,"+
					"FOREIGN KEY(FS_waggonID) REFERENCES Waggon(waggonID));";
		try {
			stmt=c.prepareStatement(sql);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void createTBuchung()
	{
		PreparedStatement stmt;
		String sql ="CREATE TABLE IF NOT EXISTS Buchung("+
					"kundenID INTEGER PRIMARY KEY AUTOINCREMENT,"+
					"name TEXT NOT NULL,"+
					"FS_sitzplatz INT NOT NULL,"+
					"FOREIGN KEY(FS_sitzplatz) REFERENCES Sitzplatz(sitzplatz));";
		try {
			stmt=c.prepareStatement(sql);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void showdata1()
	{
		System.out.println("Anzahl");
		String show = "SELECT COUNT(distinct Buchung.kundenID) "+
					  "AS Anzahl FROM Buchung";
		try
		{
			Statement stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery(show);
			while(rs.next())
			{
				System.out.println(rs.getInt(1));
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void showdata2()
	{
		System.out.println("Name-Sitzplatz-Klasse");
		String show = "SELECT a.name, b.sitzplatz, c.klasse "+
					  "FROM Buchung a "+
					  "INNER JOIN Sitzplatz b "+
					  "ON a.FS_sitzplatz=b.sitzplatz "+
					  "INNER JOIN Waggon c "+
					  "ON b.FS_waggonID=c.waggonID";
		try
		{
			Statement stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery(show);
			while(rs.next())
			{
				System.out.println(rs.getString(1)+"-"+rs.getInt(2)+"-"+rs.getInt(3));
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void showdata3()
	{
		System.out.println("Klasse-Anzahl");
		String show = "SELECT c.klasse, COUNT(a.kundenID) "+
				"FROM Buchung a "+
				"INNER JOIN Sitzplatz b "+
				"ON a.FS_sitzplatz=b.sitzplatz "+
				"INNER JOIN Waggon c "+
				"ON b.FS_waggonID=c.waggonID "+
				"GROUP BY c.klasse";
		try
		{
			Statement stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery(show);
			while(rs.next())
			{
				System.out.println(rs.getInt(1)+"-"+rs.getInt(2));
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void createWaggon() 
	{
		System.out.println("Geben sie die Klasse des Waggons ein.");
		int k= sc.nextInt();
		if(k>3)
		{
			k=3;
		}
		if(k<1)
		{
			k=1;
		}
		PreparedStatement stmt=null;
		try{
			String sql="INSERT INTO Waggon(klasse) VALUES(?)";
			stmt=c.prepareStatement(sql);
			stmt.setInt(1, k);
			stmt.executeUpdate();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public void createSitzplatz()
	{
		System.out.println("Geben sie die ID des Waggons ein, in dem sich der Sitzplatz befindet.");
		int k= sc.nextInt();
		PreparedStatement stmt=null;
		try{
			String sql="INSERT INTO Sitzplatz(FS_waggonID) VALUES(?)";
			stmt=c.prepareStatement(sql);
			stmt.setInt(1, k);
			stmt.executeUpdate();
		}
		catch(SQLException e){
			System.out.println("Diesen Waggon gibt es leider nicht.");
			e.printStackTrace();
		}
	}
	
	public void createBuchung()
	{
		System.out.println("Geben sie die ID des Sitzplatzes ein.");
		int k= sc.nextInt();
		System.out.println("Geben sie den Namen der Person ein.");
		String j= sc.nextLine();
		PreparedStatement stmt=null;
		try{
			String sql="INSERT INTO Buchung(name, FS_sitzplatz)  VALUES(?,?)";
			stmt=c.prepareStatement(sql);
			stmt.setString(1, j);
			stmt.setInt(2, k);
			stmt.executeUpdate();
		}
		catch(SQLException e){
			System.out.println("Diesen Sitzplatz gibt es leider nicht.");
			e.printStackTrace();
		}
	}
}
