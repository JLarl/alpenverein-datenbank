import java.util.Scanner;

//Es gibt leider noch einen Fehler mit dem Scanner in der Methode db.createBuchung().
//Dort überspringt der Scanner einfach den 2. Wert den dieser einlesen soll.


public class Zug_Main { 
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		DBManager db = new DBManager();
		db.createTWaggon();
		db.createTSitzplatz();
		db.createTBuchung();
		while (true) {
			System.out.println("Auswahl:");
			System.out.println("1-Daten anzeigen lassen");
			System.out.println("2-Waggon erstellen");
			System.out.println("3-Sitzplatz erstellen");
			System.out.println("4-Buchung");
			String st = sc.next();
			switch (st) {
			case "1":
				db.showdata1();
				db.showdata2();
				db.showdata3();
				break;

			case "2":
				db.createWaggon();
				break;

			case "3":
				db.createSitzplatz();
				break;

			case "4":
				db.createBuchung();
			}
		}

	}

}
