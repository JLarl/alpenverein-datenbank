import java.sql.Timestamp;

public class Verbrauch {

	private double value;
	private Timestamp time;
	
	public Verbrauch(long time, double value)
	{
		this.value = value;
		this.time = new Timestamp(time);
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}
	

}
