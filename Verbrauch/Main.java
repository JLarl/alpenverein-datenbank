import java.sql.Timestamp;
import java.time.LocalDate;

public class Main {

	public static void main(String[] args) {
		DBManager db = new DBManager("strom_verbrauch");
		LocalDate start = db.getLast(new Timestamp(0)).getTime().toLocalDateTime().toLocalDate();
		LocalDate end = db.getFirst(new Timestamp(System.currentTimeMillis())).getTime().toLocalDateTime().toLocalDate();
		int i = 1;
		double a = 0;
		double b = 0;
		for(LocalDate date = start.plusMonths(1); date.isBefore(end.plusMonths(1)); date.plusMonths(1),i++)
		{
			Timestamp firstDay = Timestamp.valueOf(date.withDayOfMonth(1).atStartOfDay());
			double value = getiVerbrauch(db.getFirst(firstDay), db.getLast(firstDay), firstDay);
			if(!db.getFirst(firstDay).getTime().equals(firstDay))
			{
				db.insertVerbrauch(new Verbrauch(firstDay.getTime(),value));
			}
			if (a != 0) {
				double verbrauch = value - a;
				System.out.println("Absolute consumption " + i +": " + verbrauch);
				b += verbrauch;
			}
			a = value;
		}
		System.out.println("Average consumption: " + b);
	}

	private static double getiVerbrauch(Verbrauch v1, Verbrauch v2, Timestamp firstDay)
	{
		double a = (v2.getValue()-v1.getValue())/(v2.getTime().getTime()-v1.getTime().getTime());
		double b = v2.getValue()-v2.getTime().getTime()*a;
		double c = a*firstDay.getTime()+b;
		return c;		
	}
}
