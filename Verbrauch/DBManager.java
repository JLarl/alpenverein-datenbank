import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class DBManager {

	private Connection c;
	private String table;
	
	public DBManager(String table)
	{
		this.table =table;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/sqlite-tools-x86-3150100/verbrauch.db");
		}
		catch(SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	public Verbrauch getFirst(Timestamp ts) {
		String sql = "SELECT MAX(datum),value FROM " + table + " WHERE datum <= ?";
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try{
			stmt=c.prepareStatement(sql);
			stmt.setLong(1, ts.getTime()/1000);
			rs=stmt.executeQuery();
			rs.next();
			return new Verbrauch(rs.getLong(1)*1000, rs.getDouble(2));
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(stmt != null && rs != null)
			{
				try {
					stmt.close();
					rs.close();
				}
				catch(SQLException e)
				{
					e.printStackTrace();
				}
			}
			
		}
		return null;
	}
	
	public Verbrauch getLast(Timestamp ts) {
		String sql = "SELECT MAX(datum),value FROM " + table + " WHERE datum > ?";
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try{
			stmt=c.prepareStatement(sql);
			stmt.setLong(1, ts.getTime()/1000);
			rs=stmt.executeQuery();
			rs.next();
			return new Verbrauch(rs.getLong(1)*1000, rs.getDouble(2));
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(stmt != null && rs != null)
			{
				try {
					stmt.close();
					rs.close();
				}
				catch(SQLException e)
				{
					e.printStackTrace();
				}
			}
			
		}
		return null;
	}
	public void createVerbrauch2() throws SQLException
	{
		PreparedStatement stmt;
		String sql = "CREATE TABLE IF NOT EXISTS verbrauch2("+
					 "value REAL NOT NULL);";
		stmt=c.prepareStatement(sql);
		stmt.executeUpdate();
		stmt.close();
	}
	
	public void insertVerbrauch(Verbrauch verbrauch)
	{
		PreparedStatement stmt=null;
		try{
			String sql="INSERT INTO verbrauch2 VALUES(?,?)";
			stmt=c.prepareStatement(sql);
			stmt.setLong(1, verbrauch.getTime().getTime()/1000);
			stmt.setDouble(2, verbrauch.getValue());
			stmt.executeUpdate();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		finally
		{
			if(stmt != null) {
				try{
					stmt.close();
				}
				catch(SQLException e)
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	public void close()
	{
		try{
			c.close();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
}
