import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class AlpenvereinMAIN {

	//Ich konnte das Programm leider nicht testen, da ich einen Fehler beim kreeiren einer Datenbank habe.
	//Außerdem war ich mir bei der Verwendung von den Fremdschlüsseln nicht sicher und daher wurden diese nicht eingebaut.
	//Letztlich war ich mir nicht sicher, wie man Dates schreiben soll. (Ich habe sie hier einfach mit int geschrieben.)
	//Im Datenbankmodell und in dem CREATE txtFile sind die Fremdschlüssel enthalten.
	
	private static Connection con;
	private static Scanner sc = new Scanner(System.in);

	AlpenvereinMAIN() throws SQLException {
		try {
			Class.forName("org.sqlite.JDBC");
			con = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/sqlite-tools-win32-x86-3150100/Alpenverein.db");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws SQLException {
		new AlpenvereinMAIN();
		create();
		while(true)		//Bei Falscheingabe wird die Aufforderung wiederholt.
		{
			System.out.println("1: Insert; 2: Delete; 3: Show");
			String s = sc.next();
			if(s.equals("1"))	// ".equals()" statt "==", da bei "==" die Referenzposition im Speicher verglichen wird.
			{
				insert();
			}
			if(s.equals("2"))
			{
				delete();
			}
			if(s.equals("3"))
			{
				showTables();
			}
		}

	}
	
	public static void create() throws SQLException
	{
		PreparedStatement stmt;
		
		String s1 = "CREATE TABLE IF NOT EXISTS Unterkuenfte("+
					"UnterkunftID INT PRIMARY KEY,"+
					"Baujahr INT NOT NULL,"+
					"Unterkunftname Text NOT NULL,"+
					"Meereshoehe INT NOT NULL,"+
					"Zimmerzahl INT NOT NULL);";
		stmt = con.prepareStatement(s1);
		stmt.executeUpdate();
		String s2 = "CREATE TABLE IF NOT EXISTS Mitglieder("+
					"MitgliedsID INT PRIMARY KEY,"+
					"Vorname Text NOT NULL,"+
					"Nachname Text NOT NULL,"+
					"Geburtsdatum date NOT NULL,"+
					"Wohnort Text NOT NULL,"+
					"Postleitzahl Text NOT NULL,"+
					");";
		stmt = con.prepareStatement(s2);
		stmt.executeUpdate();
		String s3 = "CREATE TABLE IF NOT EXISTS Kurse("+
					"KursID INT PRIMARY KEY,"+
					"Kursname Text NOT NULL,"+
					"Kursstart date NOT NULL,"+
					"Kursende date NOT NULL,"+
					"Kursart Text NOT NULL"+
					");";
		stmt = con.prepareStatement(s3);
		stmt.executeUpdate();
		stmt.close();
	}
	
	public static void insert()
	{		//Bei Falscheingabe kommt man einfach wieder zur Anfangsaufforderung zurück.
		System.out.println("A: Unterkünfte; B: Mitglieder; C:Kurse");	
		PreparedStatement stmt=null;
		String s = sc.next();
		try{
			if(s.equals("A"))
			{
				String sIN = "INSERT INTO Unterkuenfte VALUES (?,?,?,?,?)";
				stmt=con.prepareStatement(sIN);
				System.out.println("Bitte eingeben: UnterkunftID");
				stmt.setInt(1, sc.nextInt());
				System.out.println("Bitte eingeben: Baujahr");
				stmt.setInt(2, sc.nextInt());
				System.out.println("Bitte eingeben: Unterkunftname");
				stmt.setString(3, sc.nextLine());
				System.out.println("Bitte eingeben: Meereshöhe");
				stmt.setInt(4, sc.nextInt());
				System.out.println("Bitte eingeben: Zimmerzahl");
				stmt.setInt(5, sc.nextInt());
			}
			if(s.equals("B"))
			{
				String sIN = "INSERT INTO Mitglieder VALUES (?,?,?,?,?,?)";
				stmt=con.prepareStatement(sIN);
				System.out.println("Bitte eingeben: MitgliedsID");
				stmt.setInt(1, sc.nextInt());
				System.out.println("Bitte eingeben: Vorname");
				stmt.setString(2, sc.nextLine());
				System.out.println("Bitte eingeben: Nachname");
				stmt.setString(3,sc.nextLine());
				System.out.println("Bitte eingeben: Geburtsdatum");
				stmt.setInt(4, sc.nextInt());
				System.out.println("Bitte eingeben: Wohnort");
				stmt.setString(5, sc.nextLine());
				System.out.println("Bitte eingeben: Postleitzahl");
				stmt.setString(6, sc.nextLine());
			}
			if(s.equals("C"))
			{
				String sIN = "INSERT INTO Kurse VALUES (?,?,?,?,?)";
				stmt=con.prepareStatement(sIN);
				System.out.println("Bitte eingeben: KursID");
				stmt.setInt(1, sc.nextInt());
				System.out.println("Bitte eingeben: Kursname");
				stmt.setString(2, sc.nextLine());
				System.out.println("Bitte eingeben: Kursstart");
				stmt.setInt(3, sc.nextInt());
				System.out.println("Bitte eingeben: Kurseende");
				stmt.setString(4, sc.nextLine());
				System.out.println("Bitte eingeben: Kursart");
				stmt.setString(5, sc.nextLine());
			}
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public static void delete()
	{
		PreparedStatement stmt = null;
		System.out.println("A: Unterkuenfte; B: Mitglieder; C: Kurse");
		String s = sc.next();
		System.out.println("ID:");
		int i = sc.nextInt();
		
		try
		{
			if(s=="A")
			{
				String sDEL= "DELETE FROM Unterkuenfte WHERE UnterkunftID = " + i;
				stmt = con.prepareStatement(sDEL);
				stmt.executeUpdate();
			}
			if(s=="B")
			{
				String sDEL= "DELETE FROM Mitglieder WHERE MitgliedsID = " + i;
				stmt = con.prepareStatement(sDEL);
				stmt.executeUpdate();
			}
			if(s=="C"){
				String sDEL= "DELETE FROM Kurse WHERE KursID = " + i;
				stmt = con.prepareStatement(sDEL);
				stmt.executeUpdate();
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public static void showTables() throws SQLException
	{
		System.out.println("A: Unterkünfte; B: Mitglieder; C: Kurse");
		String s = sc.next();
		String show = "SELECT * FROM " + s + ";";
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(show);
		try
		{
			while(rs.next())
			{
				if(s.equals("A"))
				{
					System.out.println("UnterkunftsID: " + rs.getInt(1));
					System.out.println("Baujahr: " + rs.getInt(2));
					System.out.println("Unterkunftname: " +rs.getString(3));
					System.out.println("Meereshöhe: " + rs.getInt(4));
					System.out.println("Zimmerzahl: " + rs.getInt(5));
				}
				if(s.equals("B"))
				{
					System.out.println("MitgliedsID: " + rs.getInt(1));
					System.out.println("Vorname: " + rs.getString(2));
					System.out.println("Nachname: " + rs.getString(3));
					System.out.println("Geburtsdatum: " + rs.getDate(4));
					System.out.println("Wohnort: " + rs.getString(5));
					System.out.println("Postleitzahl: " + rs.getString(6));
				}
				if(s.equals("C"))
				{
					System.out.println("KursID: " + rs.getInt(1));
					System.out.println("Kursname: " + rs.getString(2));
					System.out.println("Kursstart: " + rs.getDate(3));
					System.out.println("Kursene: " + rs.getDate(4));
					System.out.println("Kursart: " + rs.getString(5));
				}
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		rs.close();
		stmt.close();
	}

}
