meiste Heimtore
SELECT vorname, nachname FROM Tor t, Spiel s, SpieltFür sf, Spieler sp WHERE t.spielID=s.spielID AND s.heimMannschaft=sf.vereinsname AND sf.bis=0 AND sf.spielerPassNr=t.spielerPassNr AND sp.spielerPassNr=t.spielerPassNr GROUP BY t.spielerPassNr ORDER BY count(t.spielerPassNr) desc LIMIT 1;  

öftester wechsel
SELECT vorname, nachname FROM Spieler s, SpieltFür sf WHERE s.spielerPassNr=sf.spielerPassNr GROUP BY sf.spielerPassNr ORDER BY count(sf.spielerPassNr) desc LIMIT 1;

schiedsrichter mit meisten roten karten
SELECT vorname, nachname FROM Karten k, Spiel s,Schiedsrichter sc  WHERE zulassungsnummer=schiedsrichter AND k.spielID=s.spielID AND farbe="Rot" GROUP BY sc.zulassungsnummer ORDER BY count(sc.zulassungsnummer) desc LIMIT 1;

summe tore
SELECT count() FROM Tor;

durchschnittstore
SELECT CAST(COUNT() as REAL)/(SELECT CAST(COUNT() as REAL) FROM Spiel) FROM Tor;

meist karten erhalten
SELECT vorname, nachname FROM Spieler s, Karten k WHERE s.spielerPassNr=k.spielerPassNr  GROUP BY k.spielerPassNr ORDER BY count(k.spielerPassNr) desc LIMIT 1;

Torstärkste woche
SELECT strftime("%Y%W", datum) as week, sum(toreGästeMannschaft+toreHeimMannschaft) as tore FROM Spiel GROUP BY week ORDER BY tore desc LIMIT 1;

Tore pro saison
SELECT strftime("%Y", datum) as saison, sum(toreGästeMannschaft+toreHeimMannschaft) as tore FROM Spiel GROUP BY saison;
