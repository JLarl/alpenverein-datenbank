import java.util.Arrays;

public class MAIN {
	final static int AMOUNTOFSAMECOLORS = 13;
	final static int AMOUNTOFCOLORS = 4;

	public static void main(String[] args) {
		int ran = 0;
		int swap = 0;
		int[] cards = new int[(AMOUNTOFSAMECOLORS * AMOUNTOFCOLORS)];
		int fHCount = 0;
		int fCount = 0;
		int sCount = 0;
		int tOAKCount = 0;
		int tPCount = 0;
		int oPCount = 0;
		DBManager db1= new DBManager();
		DBManager.getInstance();
		
		long timestamp1 = System.currentTimeMillis();
		
		for(int g = 0; g<10000; g++)
		{
		int[] hand = new int[5];
		for (int as = 0; as < 52; as++) {
			cards[as] = as+1;
		}

		for (int i = 0; i < 5; i++) {
			ran = (int) (Math.random() * (52 - i));
			hand[i] = cards[ran];
			swap = cards[51-i];
			cards[51 - i] = hand[i];
			cards[ran] = swap;
			ran = swap;
			//System.out.println(hand[i]+ " "+ cardValue(hand[i])+ " "+ cardColor(hand[i]));
		}
		if(fullHouse(hand))
		{
			fHCount=fHCount+1;
		}
		if(flush(hand))
		{
			fCount=fCount+1;
		}
		if(straight(hand))
		{
			sCount=sCount+1;
		}

		if(threeOfAKind(hand))
		{
			tOAKCount=tOAKCount+1;
		}
		if(twoPair(hand))
		{
			tPCount=tPCount+1;
		}
		if(onePair(hand))
		{
			oPCount=oPCount+1;
		}
		}
		long timestamp2 = System.currentTimeMillis();
		long time = timestamp2-timestamp1;
		System.out.println("Dauer (in Millisekunden): "+time);
		db1.addAttempt(timestamp1, oPCount, tPCount, tOAKCount, sCount, fCount, fHCount);
		db1.close();
	}
	
	public static int cardValue(int card) {
		int wert = card%AMOUNTOFSAMECOLORS;
		if(card==13||card==26||card==39||card==52)
		{
			wert=13;
		}
		return wert;
	}

	public static int cardColor(int card) {
		if(card>0&&card<14)
		{
			return 0;
		}
		if(card>13&&card<27)
		{
			return 1;
		}
		if(card>26&&card<40)
		{
			return 2;
		}
		else return 3;
	}
	public static boolean onePair(int[] hand) {
		for (int i = 0; i < hand.length - 1; i++) {
			for (int j = (i + 1); j < hand.length; j++) {
				if (cardValue(hand[i]) == cardValue(hand[j])) {
					return true;
				}
			}
		}
		return false;
	}
	public static boolean twoPair(int[] hand) {
		int paar1 = 0;
		for (int i = 0; i < hand.length - 1; i++) {
			for (int j = (i + 1); j < hand.length; j++) {
				if (cardValue(hand[i]) == cardValue(hand[j])) {
					paar1 = cardValue(hand[i]);
				}
			}
		}
		for (int i = 0; i < hand.length - 1; i++) {
			for (int j = (i + 1); j < hand.length; j++) {
				if (cardValue(hand[i]) == cardValue(hand[j]) && !(cardValue(hand[i]) == paar1)) {
					return true;
				}
			}
		}
		return false;
	}
	public static boolean threeOfAKind(int[] hand) {
		for (int i = 0; i < hand.length - 2; i++) {
			for (int j = (i + 1); j < hand.length - 1; j++) {
				for (int k = (i + 2); k < hand.length; k++) {
					if (cardValue(hand[i]) == cardValue(hand[j]) && cardValue(hand[i]) == cardValue(hand[k])) {
						return true;
					}
				}
			}
		}
		return false;
	}
	public static boolean straight(int[] hand) {
		Arrays.sort(hand);
		for (int i = 1; i < hand.length - 1; i++) {
			if (cardValue(hand[i]) - cardValue(hand[i - 1]) != 1) {
				return false;
			}
		}
		return true;
	}
	public static boolean flush(int[] hand) {
		for (int i = 0; i < hand.length-1; i++) {
			if (cardColor(hand[i]) != cardColor(hand[i + 1])) {
				return false;
			}
		}
		return true;
	}
	public static boolean fullHouse(int[] hand) {
		int paar1 = 0;
		for (int i = 0; i < hand.length - 1; i++) {
			for (int j = (i + 1); j < hand.length; j++) {
				if (cardValue(hand[i]) == cardValue(hand[j])) {
					paar1 = cardValue(hand[i]);
				}
			}
		}
		for (int i = 0; i < hand.length - 2; i++) {
			for (int j = (i + 1); j < hand.length - 1; j++) {
				for (int k = (i + 2); k < hand.length; k++) {
					if (cardValue(hand[i]) == cardValue(hand[j]) && cardValue(hand[i]) == cardValue(hand[k])
							&& !(cardValue(hand[i]) == paar1)) {
						return true;
					}
				}
			}
		}
		return false;
	}
}
