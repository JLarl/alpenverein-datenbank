import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBManager {
	private Connection c;
	private static DBManager db;

	DBManager() {
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/sqlite-tools-win32-x86-3150100/Poker.db");
		} catch (SQLException | ClassNotFoundException e) {
			System.out.println("Connection failed.");
		}
	}

	public static DBManager getInstance() {
		if (db == null) {
			db = new DBManager();
		}
		return db;
	}

	
	public void addAttempt(long timestamp1, int oPCount, int tPCount, int tOAKCount, int sCount, int fCount, int fHCount)
	{
		PreparedStatement stmt =null;
		String sql="INSERT INTO ATTEMPT VALUES(?,?,?,?,?,?,?)";
		try{
			stmt=c.prepareStatement(sql);
			stmt.setLong(1, timestamp1);
			stmt.setInt(2, oPCount);
			stmt.setInt(3, tPCount);
			stmt.setInt(4, tOAKCount);
			stmt.setInt(5, sCount);
			stmt.setInt(6, fCount);
			stmt.setInt(7, fHCount);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(stmt != null)
			{
				try
				{
					stmt.close();
				}
				catch(SQLException e)
				{
					e.printStackTrace();
				}
			}
		}
		System.out.println("Daten wurden eingetragen.");
	}
	public void close()
	{
		try{
			c.close();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
}
